#include "BNO055.h"

BNO055::BNO055(int BNO_id, int BNO_address, String _topic_name) {
    imu_topic_name = hardware_name + "/IMU/" + _topic_name + "/imu";
    magnetometer_topic_name = hardware_name + "/IMU/" + _topic_name + "/magnetometer";
    temperature_topic_name = hardware_name + "/IMU/" + _topic_name + "/temperature";

    bno = Adafruit_BNO055(BNO_id, BNO_address, &Wire);
    /* Initialise the sensor */
//    if (!bno.begin()) {
//        /* There was a problem detecting the BNO055 ... check your connections */
//        // Serial.print("Ooops, no BNO055 detected ... Check your wiring or I2C ADDR!");
//    }
}

BNO055::~BNO055() {
    delete imu_publisher;
    delete magnetometer_publisher;
    delete temperature_publisher;
}

void BNO055::setup_ros(NodeHandle& _nh) {
    imu_publisher = new ros::Publisher(imu_topic_name.c_str(), &imu_msg);
    _nh.advertise(*imu_publisher);

    magnetometer_publisher = new ros::Publisher(magnetometer_topic_name.c_str(), &magnetic_msg);
    _nh.advertise(*magnetometer_publisher);

    temperature_publisher = new ros::Publisher(temperature_topic_name.c_str(), &temperature_msg);
    _nh.advertise(*temperature_publisher);
}

void BNO055::update() {
    update_temperature();
    return;
    update_magnetometer();
    update_IMU();
}

void BNO055::update_temperature() {
    temperature_msg.temperature = bno.getTemp();
    temperature_publisher->publish(&temperature_msg);
}

void BNO055::update_IMU() {
    update_orientation();
    update_gyroscope();
    update_accelerometer();
    imu_publisher->publish(&imu_msg);
}

void BNO055::update_orientation() {
    sensors_event_t event;
    bno.getEvent(&event, Adafruit_BNO055::VECTOR_EULER);

    imu_msg.orientation.x = event.orientation.x;
    imu_msg.orientation.y = event.orientation.y;
    imu_msg.orientation.z = event.orientation.z;
    // TODO: maybe add orientation.w
}

void BNO055::update_gyroscope() {
    sensors_event_t event;
    bno.getEvent(&event, Adafruit_BNO055::VECTOR_GYROSCOPE);

    imu_msg.angular_velocity.x = event.gyro.x;
    imu_msg.angular_velocity.y = event.gyro.y;
    imu_msg.angular_velocity.z = event.gyro.z;
}

void BNO055::update_accelerometer() {
    sensors_event_t event;
    bno.getEvent(&event, Adafruit_BNO055::VECTOR_ACCELEROMETER);

    imu_msg.linear_acceleration.x = event.acceleration.x;
    imu_msg.linear_acceleration.y = event.acceleration.y;
    imu_msg.linear_acceleration.z = event.acceleration.z;
}

void BNO055::update_magnetometer() {
    sensors_event_t event;
    bno.getEvent(&event, Adafruit_BNO055::VECTOR_MAGNETOMETER);

    magnetic_msg.magnetic_field.x = event.magnetic.x;
    magnetic_msg.magnetic_field.y = event.magnetic.y;
    magnetic_msg.magnetic_field.z = event.magnetic.z;

    magnetometer_publisher->publish(&magnetic_msg);
}
