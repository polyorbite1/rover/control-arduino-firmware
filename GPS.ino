#include "GPS.h"

#include <SoftwareSerial.h>

GPS::GPS(int tx_pin, int rx_pin, String _topic_name){
  topic_name = _topic_name;

  SoftwareSerial mySerial(tx_pin, rx_pin);
  gps = new Adafruit_GPS(&mySerial);
  gps->begin(9600);

  //gps->sendCommand(PMTK_SET_NMEA_OUTPUT_RMCGGA);
  // uncomment this line to turn on only the "minimum recommended" data
  gps->sendCommand(PMTK_SET_NMEA_OUTPUT_RMCONLY);

  gps->sendCommand(PMTK_SET_NMEA_UPDATE_1HZ);
  gps->sendCommand(PGCMD_ANTENNA);

  refresh_timer = millis();
}

GPS::~GPS(){
  delete publish_gps;
  delete publish_north;
  delete publish_speed;
}

void GPS::setup_ros(NodeHandle &nh) {
  String url = hardware_name+"/GPS/"+topic_name;
  
  publish_gps = new ros::Publisher((topic_name+"/lon_lat").c_str(), &gps_message);
  nh.advertise(*publish_gps);
  
  publish_north = new ros::Publisher((topic_name+"/north").c_str(), &north_message);
  nh.advertise(*publish_north);  
  
  publish_speed = new ros::Publisher((topic_name+"/speed").c_str(), &speed_message);
  nh.advertise(*publish_speed);
}

void GPS::update(){
  if (millis()-refresh_timer < GPS_REFRESH_TIME)
    return;

  if (gps->newNMEAreceived()) {
    if (!gps->parse(gps->lastNMEA()))
      return;  // we can fail to parse a sentence in which case we should just wait for another
  }

  if (!gps->fix)
    return;

  gps_message.latitude = gps->latitude;
  gps_message.longitude = gps->longitude;
  gps_message.altitude = gps->altitude;
  north_message.data = gps->speed * 0.514444; //speed in kn to m/s
  speed_message.data = gps->angle;

  publish_gps->publish(&gps_message);
  publish_north->publish(&north_message);
  publish_speed->publish(&speed_message);

  refresh_timer = millis();
}
