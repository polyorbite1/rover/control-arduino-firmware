#include "Fan.h"
#include "pinout_pro_v1.h"

// rpm count: https://fdossena.com/?p=ArduinoFanControl/i.md

Fan::Fan(int _PWM_pin, int _sense_pin, String _topic_name) {
    PWM_pin = _PWM_pin;
    sense_pin = _sense_pin;

    pinMode(PWM_pin, OUTPUT);
    analogWrite(PWM_pin, 0);
    pinMode(sense_pin, INPUT_PULLUP); //set the sense pin as input with pullup resistor
    rpm_topic_name = hardware_name + "/fan/" + _topic_name + "/rpm";
    velocity_topic_name = hardware_name + "/fan/" + _topic_name + "/velocity";
    analogWrite(PWM_pin, 255);
}

Fan::~Fan() {
    delete publish_speed;
    delete subscribe_speed;
}

void Fan::update() {
    update_RPM();
    update_ros();
}

void Fan::setup_ros(NodeHandle& nh) {
    publish_speed = new ros::Publisher(rpm_topic_name.c_str(), &speed_message);
    nh.advertise(*publish_speed);

    subscribe_speed = new ros::Subscriber<std_msgs::UInt8, Fan>(velocity_topic_name.c_str(), &Fan::move_at, this);
    nh.subscribe(*subscribe_speed);
}

void Fan::update_ros() {
    speed_message.data = rpm;
    publish_speed->publish(&speed_message);
}

void Fan::move_at(const std_msgs::UInt8& msg) {
    analogWrite(PWM_pin, msg.data);
}

void Fan::sense_interrupt() {
    time_sense_1 = time_sense_2;
    time_sense_2 = millis();
}

void Fan::update_RPM() {
    if (time_sense_2 == 0) {
        rpm = 0;
    } else if (millis() > time_sense_2 + STUCK_THRESHOLD) {
        rpm = 0;
    } else {
        rpm = 60000 / (time_sense_2 - time_sense_1) / 2;
    }
}
