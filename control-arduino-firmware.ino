// To avoid bug with rosserial
// #define USE_USBCON

#include <ros.h>
// default is 25, 25, 512, 512 -> subscriber limit, publisher limit, subscriber buffer size, publisher buffer size
typedef ros::NodeHandle_<ArduinoHardware, 30, 20, 512, 512> NodeHandle;

// #include <SoftwareSerial.h>

#include "Component.h"

#include "BatteryMonitor.h"
#include "bno055.h"
#include "Encoder.h"
#include "Fan.h"
//#include "GPS.h"
#include "Motor.h"
#include "Switch.h"
// #include "Ultrason.h"

#include "pinout_pro_v1.h"

NodeHandle ros_handle;

// ROS naming convention: hardware_name/component_type/component_name/[get|set]/value_name

Component* components[] = {
        new Fan(FAN_1_PWM, FAN_1_INT, "intake1"),
        new Fan(FAN_2_PWM, FAN_2_INT, "intake2"),
        new Fan(FAN_3_PWM, FAN_3_INT, "outake1"),
        new Fan(FAN_4_PWM, FAN_4_INT, "outake2"),
        new Motor(MOTOR_RR_PWM, MOTOR_RR_DIR, false, "RR"), // Rear   Right
        new Motor(MOTOR_RL_PWM, MOTOR_RL_DIR, true, "RL"), // Rear   Left
        new Motor(MOTOR_CR_PWM, MOTOR_CR_DIR, false, "CR"), // Center Right
        new Motor(MOTOR_CL_PWM, MOTOR_CL_DIR, true, "CL"), // Center Left
        new Motor(MOTOR_FR_PWM, MOTOR_FR_DIR, false, "FR"), // Front  Right
        new Motor(MOTOR_FL_PWM, MOTOR_FL_DIR, true, "FL"), // Front  Left
        new Encoder(MOTOR_RR_ChA, MOTOR_RR_ChB, false, "RR"), // Rear   Right
        new Encoder(MOTOR_RL_ChA, MOTOR_RL_ChB, true, "RL"), // Rear   Left
        new Encoder(MOTOR_CR_ChA, MOTOR_CR_ChB, false, "CR"), // Center Right
        new Encoder(MOTOR_CL_ChA, MOTOR_CL_ChB, true, "CL"), // Center Left
        new Encoder(MOTOR_FR_ChA, MOTOR_FR_ChB, false, "FR"), // Front  Right
        new Encoder(MOTOR_FL_ChA, MOTOR_FL_ChB, true, "FL"), // Front  Left
        new Switch(LIGHT_RED, "red"),
        new Switch(LIGHT_ORANGE, "orange"),
        new Switch(LIGHT_GREEN, "green"),
        new Switch(LIGHT_BLUE, "blue"),
        new Switch(LIGHT_BUZZER, "buzzer"),
        new Switch(LIGHT_FRONT, "light_front"),
//        new GPS(GPS_Tx, GPS_Rx, "serial_gps"),
        // new BNO055(BNO_ID, BNO_ADDRESS, "BNO055"),
        new BatteryMonitor(VOLTAGE_READING_PIN, CURRENT_READING_PIN, "lithium_24v"),

//         new Switch(SPARE, "spare"),
//         new Ultrason(TRIGGER_PIN, ECHO_PIN, 200, "ultrason"),
};

void handle_interrupt_intake1() { ((Fan*) components[0])->sense_interrupt(); }

void handle_interrupt_intake2() { ((Fan*) components[1])->sense_interrupt(); }

void handle_interrupt_outtake1() { ((Fan*) components[2])->sense_interrupt(); }

void handle_interrupt_outtake2() { ((Fan*) components[3])->sense_interrupt(); }

void setup() {
    Serial.begin(500000);
    ros_handle.getHardware()->setBaud(500000);
    delay(100);

    //set tachISR to be triggered when the signal on the sense pin goes low
    attachInterrupt(FAN_1_INT, handle_interrupt_intake1, FALLING);
    attachInterrupt(FAN_2_INT, handle_interrupt_intake2, FALLING);
    attachInterrupt(FAN_3_INT, handle_interrupt_outtake1, FALLING);
    attachInterrupt(FAN_4_INT, handle_interrupt_outtake2, FALLING);

    ros_handle.initNode();

    for (Component* component: components)
        component->setup_ros(ros_handle);
}

void loop() {
    for (Component* component: components)
        component->update();

    ros_handle.spinOnce();

    delay(5);
}
