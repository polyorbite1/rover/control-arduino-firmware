#pragma once
//TODO: fix topic name

#include <sensor_msgs/NavSatFix.h>
#include <std_msgs/Float32.h>
#include <Adafruit_GPS.h>

#include "Component.h"

#define GPS_REFRESH_TIME 1000 //tim ein millis

class GPS : public Component {
  public:
    GPS(int tx_pin, int rx_pin, String _topic_name);
    ~GPS();

    void setup_ros(NodeHandle &_nh) override;
    void update() override;

  private:
    Adafruit_GPS *gps;
    uint32_t refresh_timer;

    String topic_name;

    ros::Publisher* publish_gps;
    sensor_msgs::NavSatFix gps_message;

    ros::Publisher* publish_north;
    std_msgs::Float32 north_message;

    ros::Publisher* publish_speed;
    std_msgs::Float32 speed_message;
};
