#pragma once
//TODO: fix topic name

#include <Wire.h> // TODO : verify if necessary 
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h> // TODO : verify if necessary

#include <sensor_msgs/Imu.h>
#include <sensor_msgs/MagneticField.h>
#include <sensor_msgs/Temperature.h>

#include "Component.h"

class BNO055 : public Component{
  public:
    BNO055(int BNO_id, int BNO_address, String _topic_name);

    BNO055(String _distance_topic_name, String _velocity_topic_name);
    ~BNO055();

    void setup_ros(NodeHandle &_nh) override;
    void update() override;

  protected:
    // Check I2C device address and correct line below (by default address is 0x29 or 0x28)
    Adafruit_BNO055 bno;

    // ROS STUFF
    ros::Publisher* imu_publisher;
    ros::Publisher* magnetometer_publisher;
    ros::Publisher* temperature_publisher;

  private:
    // TODO: remove if not necessary 
    // String i2c_device_name; 

    sensor_msgs::Imu imu_msg;
    sensor_msgs::MagneticField magnetic_msg;
    sensor_msgs::Temperature temperature_msg;

    String imu_topic_name;
    String magnetometer_topic_name;
    String temperature_topic_name;

    void update_temperature();
    void update_magnetometer();
    void update_IMU();
    
    void update_orientation();
    void update_gyroscope();
    void update_accelerometer();

};
