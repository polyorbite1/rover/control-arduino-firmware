#include "BatteryMonitor.h"

#define R1 989
#define R2 46800
#define arduinoVcc 5

constexpr uint16_t voltage_const = arduinoVcc * (1 + R2 / R1) * 1000 / 1023;

BatteryMonitor::BatteryMonitor(int _voltage_pin, int _current_pin, String _topic_name) {
    voltage_pin = _voltage_pin;
    current_pin = _current_pin;

    voltage_topic_name = hardware_name + "/battery_monitor/" + _topic_name + "/voltage";
    current_topic_name = hardware_name + "/battery_monitor/" + _topic_name + "/current";

}

void BatteryMonitor::setup_ros(NodeHandle& nh) {
    voltage_publisher = new ros::Publisher(voltage_topic_name.c_str(), &voltage_message);
    nh.advertise(*voltage_publisher);

    current_publisher = new ros::Publisher(current_topic_name.c_str(), &current_message);
    nh.advertise(*current_publisher);
}

void BatteryMonitor::update() {
    voltage_message.data = getVoltage();
    voltage_publisher->publish(&voltage_message);
}

uint16_t BatteryMonitor::getVoltage() {
    uint16_t tensionSource = analogRead(voltage_pin) * voltage_const;
    return tensionSource;
}

BatteryMonitor::~BatteryMonitor() {
    delete voltage_publisher;
    delete current_publisher;
}
