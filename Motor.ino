#include "Motor.h"

Motor::Motor(int _PWM_pin, int _Dir_pin, bool _left, String _topic_name) {
    PWM_pin = _PWM_pin;
    Dir_pin = _Dir_pin;
    left = _left;

    pinMode(PWM_pin, OUTPUT);
    analogWrite(PWM_pin, 0);
    pinMode(Dir_pin, OUTPUT);
    digitalWrite(Dir_pin, HIGH);

    topic_name = hardware_name + "/motor/" + _topic_name + "/speed";
}

Motor::~Motor() {
    delete subscribe_speed;
}

void Motor::setup_ros(NodeHandle& nh) {
    subscribe_speed = new ros::Subscriber<std_msgs::Int8, Motor>(topic_name.c_str(), &Motor::move_at, this);
    nh.subscribe(*subscribe_speed);
}

void Motor::move_at(const std_msgs::Int8& msg) {
    char message = msg.data;

    bool forward = !(message & 0x80);
    if (left) forward = !forward; // check sens
    digitalWrite(Dir_pin, forward ? HIGH : LOW);

    char mask = message >> 0x7;
    int speed = ((((message + mask) ^ mask) << 1) & 0xFF);
    analogWrite(PWM_pin, speed);
}

void Motor::update() {}