#pragma once


#include <std_msgs/Int16.h>
#include <std_msgs/Bool.h>

#include "Component.h"

#define wheel_diameter 242 // in mm
#define pulses_per_revolution 376 // encoder pulses per revolution

class Encoder : public Component {
public:
    Encoder(int _ChA_pin, int _ChB_pin, bool _left, String _topic_name);

    ~Encoder();

    void setup_ros(NodeHandle& _nh) override;

    void update() override;

protected:
    int pulses_to_distance();

    void update_ros();

    void reset_pulses(const std_msgs::Bool& msg);

    void update_encoder_position();

    int ChA_pin, ChB_pin;
    bool left;
    bool channel_A_previous = false;

    int pulses = 0;

    // ros stuff
    String distance_topic_name;
    String reset_topic_name;

    ros::Publisher* publish_distance;
    std_msgs::Int16 distance_message;

    ros::Subscriber <std_msgs::Bool, Encoder>* subscribe_reset;
};