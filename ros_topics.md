# New ROS naming scheme

Pattern: 

`do not start name by number`

`[control|arm|science]/hardware_name/component_type/component_name/value_name`

`hardware_name` : Defined in `Component.h`

`component_type` : Each class of this repo is a component type.

`component_name` : Passed uppon instantiation.

`value_name` : Set in the class directly.

## Componenet
```control/mega```


## Batter monitor
`componenet_type`: `battery_monitor`

#### Publishers
| `value_name` | Message type    |
|--------------|-----------------|
| `voltage` | std_msgs/UInt16 |
| `current` | std_msgs/UInt16 |

Voltage unit: mV

## Encoder
`componenet_type`: `encoder`

#### Publishers
| `value_name` | Message type |
|--------------|------|
| `distance` | std_msgs/Int16 |

Distance in millimeters since last reset. Negative value means backwards movement. Constants: wheel size and encoder ticks per rotation.

#### Subscribers
| `value_name` | Message type | range |
|--------------|------|-------|
| `reset` | std_msgs/Bool | 1 |

Reset the distance counter to 0. Recommended at direction changes.

## IMU
`componenet_type`: `IMU`

#### Publishers
| `value_name` | Message type |
|--------------|------|
| `imu` | sensor_msgs/Imu |
| `magnetometer` | sensor_msgs/MagneticField |
| `temperature` | sensor_msgs/Temperature |


## Fan
`componenet_type`: `fan`

#### Publishers
| `value_name` | Message type |
|--------------|------|
| `rpm` | std_msgs/Int16 |

#### Subscribers
| `value_name` | Message type   | range |
|--------------|----------------|-------|
| `velocity` | std_msgs/UInt8 | 0-255 |
Directly sets PWM speed. Change to percentage?

## GPS
`componenet_type`: `GPS`

#### Publishers
| `value_name` | Message type |
|--------------|------|
| `lon_lat` | sensor_msgs/NavSatFix |
| `north` | std_msgs/Float32 |
| `speed` | std_msgs/Float32 |


## Motor
`componenet_type`: `motor`

#### Subscribers
| `value_name` | Message type  | range      |
|--------------|---------------|------------|
| `speed` | std_msgs/Int8 | -127->+127 |

First bit is direction (0 forward and 1 backward). next 7 is gradual speed (0 is stoped, 127 is maximum speed)


## Switch
`componenet_type`: `switch`

#### Subscribers
| `value_name` | Message type | range |
|--------------|------|-------|
| `state` | std_msgs/Bool | [01] |
| `morse` | std_msgs/String | [a-zA-Z0-9 .]* |



## Ultrason
`componenet_type`: `ultrason`

#### Publishers
| `value_name` | Message type |
|--------------|------|
| `distance` | std_msgs/Float32 |
