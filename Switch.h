#pragma once

#include "Component.h"

// EXTRA MORSE
#include <std_msgs/String.h>
#include <std_msgs/Bool.h>

#define MORSE_SHORT_DELAY 500
#define MORSE_LONG_DELAY (MORSE_SHORT_DELAY*2)

class Switch : public Component {
public:
    Switch(const unsigned int& _pinIndex, const String& _topic_name);

    ~Switch();

    void setup_ros(NodeHandle& nh) override;

    void update() override;

protected:
    void handle_ros_message(const std_msgs::Bool& msg);

    unsigned int pinIndex;

    String state;
    ros::Subscriber <std_msgs::Bool, Switch>* switch_topic_subscriber;

    // EXTRA MORSE
    String morse;

    void turnOn();

    void turnOff();

    template<size_t N>
    void flash_patern(int (& pattern)[N]);

    void morseWrite(const std_msgs::String& msg);

    ros::Subscriber <std_msgs::String, Switch>* morse_topic_subscriber;
};
