#pragma once

#include <std_msgs/UInt8.h>
#include <std_msgs/Int16.h>

#include "Component.h"

#define STUCK_THRESHOLD 100

class Fan : public Component {
public:
//rework: pwm+direction+max_speed
//max speed calc
    Fan(int _PWM_pin, int _sense_pin, String _topic_name);

    ~Fan();

    void setup_ros(NodeHandle& _nh) override;

    void update() override;

    void sense_interrupt();

protected:
    void update_ros();

    void update_RPM();

    void move_at(const std_msgs::UInt8& msg);

    int PWM_pin;
    int sense_pin;

    unsigned long volatile time_sense_1 = 0;
    unsigned long volatile time_sense_2 = 0;
    unsigned int rpm;

    // ros stuff
    String rpm_topic_name;
    String velocity_topic_name;
    std_msgs::Int16 speed_message;
    ros::Publisher* publish_speed;
    ros::Subscriber <std_msgs::UInt8, Fan>* subscribe_speed;
};
