#include "Switch.h"

Switch::Switch(const unsigned int& _pinIndex, const String& _topic_name) {
    pinIndex = _pinIndex;
    pinMode(pinIndex, OUTPUT);
    turnOff();
    // We realized that this concatenation work only in the constructor. Otherwise the topic is not visible in ros.
    state = hardware_name + "/switch/" + _topic_name + "/state";
    morse = hardware_name + "/switch/" + _topic_name + "/morse";
}

Switch::~Switch() {
    delete switch_topic_subscriber;
    delete morse_topic_subscriber;
}

void Switch::setup_ros(NodeHandle& nh) {

    switch_topic_subscriber = new ros::Subscriber<std_msgs::Bool, Switch>(state.c_str(), &Switch::handle_ros_message,
                                                                          this);
    nh.subscribe(*switch_topic_subscriber);

    morse_topic_subscriber = new ros::Subscriber<std_msgs::String, Switch>(morse.c_str(), &Switch::morseWrite, this);
    nh.subscribe(*morse_topic_subscriber);
}

void Switch::handle_ros_message(const std_msgs::Bool& msg) {
    digitalWrite(pinIndex, !msg.data);
}

void Switch::update() {}

// EXTRA MORSE

void Switch::turnOn() {
    digitalWrite(pinIndex, LOW); // Low because hardware is inversed
}

void Switch::turnOff() {
    digitalWrite(pinIndex, HIGH); // High because hardware is inversed
}

template<size_t N>
void Switch::flash_patern(int (& pattern)[N]) {
    for (int time: pattern) {
        turnOn();
        delay(time);
        turnOff();
        delay(MORSE_SHORT_DELAY);
    }
    delay(MORSE_SHORT_DELAY); // with extra short above makes long delay
}

void Switch::morseWrite(const std_msgs::String& msg) {
    const String& text = msg.data;

    turnOff();
    for (int i = 0; i < text.length(); ++i) {
        char character = text[i];

        switch (character) {
            case 'A':
            case 'a': {
                int pattern[] = {MORSE_SHORT_DELAY, MORSE_LONG_DELAY};
                flash_patern(pattern);
                break;
            }
            case 'B':
            case 'b': {
                int pattern[] = {MORSE_LONG_DELAY, MORSE_SHORT_DELAY, MORSE_SHORT_DELAY, MORSE_SHORT_DELAY};
                flash_patern(pattern);
                break;
            }
            case 'C':
            case 'c': {
                int pattern[] = {MORSE_LONG_DELAY, MORSE_SHORT_DELAY, MORSE_LONG_DELAY, MORSE_SHORT_DELAY};
                flash_patern(pattern);
                break;
            }
            case 'D':
            case 'd': {
                int pattern[] = {MORSE_LONG_DELAY, MORSE_SHORT_DELAY, MORSE_SHORT_DELAY};
                flash_patern(pattern);
                break;
            }
            case 'E':
            case 'e': {
                int pattern[] = {MORSE_SHORT_DELAY};
                flash_patern(pattern);
                break;
            }
            case 'F':
            case 'f': {
                int pattern[] = {MORSE_SHORT_DELAY, MORSE_SHORT_DELAY, MORSE_LONG_DELAY, MORSE_SHORT_DELAY};
                flash_patern(pattern);
                break;
            }
            case 'G':
            case 'g': {
                int pattern[] = {MORSE_LONG_DELAY, MORSE_LONG_DELAY, MORSE_SHORT_DELAY};
                flash_patern(pattern);
                break;
            }
            case 'H':
            case 'h': {
                int pattern[] = {MORSE_SHORT_DELAY, MORSE_SHORT_DELAY, MORSE_SHORT_DELAY, MORSE_SHORT_DELAY};
                flash_patern(pattern);
                break;
            }
            case 'I':
            case 'i': {
                int pattern[] = {MORSE_SHORT_DELAY, MORSE_SHORT_DELAY};
                flash_patern(pattern);
                break;
            }
            case 'J':
            case 'j': {
                int pattern[] = {MORSE_SHORT_DELAY, MORSE_LONG_DELAY, MORSE_LONG_DELAY, MORSE_LONG_DELAY};
                flash_patern(pattern);
                break;
            }
            case 'K':
            case 'k': {
                int pattern[] = {MORSE_LONG_DELAY, MORSE_SHORT_DELAY, MORSE_LONG_DELAY};
                flash_patern(pattern);
                break;
            }
            case 'L':
            case 'l': {
                int pattern[] = {MORSE_SHORT_DELAY, MORSE_LONG_DELAY, MORSE_SHORT_DELAY, MORSE_SHORT_DELAY};
                flash_patern(pattern);
                break;
            }
            case 'M':
            case 'm': {
                int pattern[] = {MORSE_LONG_DELAY, MORSE_LONG_DELAY};
                flash_patern(pattern);
                break;
            }
            case 'N':
            case 'n': {
                int pattern[] = {MORSE_LONG_DELAY, MORSE_SHORT_DELAY};
                flash_patern(pattern);
                break;
            }
            case 'O':
            case 'o': {
                int pattern[] = {MORSE_LONG_DELAY, MORSE_LONG_DELAY, MORSE_LONG_DELAY};
                flash_patern(pattern);
                break;
            }
            case 'P':
            case 'p': {
                int pattern[] = {MORSE_SHORT_DELAY, MORSE_LONG_DELAY, MORSE_LONG_DELAY, MORSE_SHORT_DELAY};
                flash_patern(pattern);
                break;
            }
            case 'Q':
            case 'q': {
                int pattern[] = {MORSE_LONG_DELAY, MORSE_LONG_DELAY, MORSE_SHORT_DELAY, MORSE_LONG_DELAY};
                flash_patern(pattern);
                break;
            }
            case 'R':
            case 'r': {
                int pattern[] = {MORSE_SHORT_DELAY, MORSE_LONG_DELAY, MORSE_SHORT_DELAY};
                flash_patern(pattern);
                break;
            }
            case 'S':
            case 's': {
                int pattern[] = {MORSE_SHORT_DELAY, MORSE_SHORT_DELAY, MORSE_SHORT_DELAY};
                flash_patern(pattern);
                break;
            }
            case 'T':
            case 't': {
                int pattern[] = {MORSE_LONG_DELAY};
                flash_patern(pattern);
                break;
            }
            case 'U':
            case 'u': {
                int pattern[] = {MORSE_SHORT_DELAY, MORSE_SHORT_DELAY, MORSE_LONG_DELAY};
                flash_patern(pattern);
                break;
            }
            case 'V':
            case 'v': {
                int pattern[] = {MORSE_SHORT_DELAY, MORSE_SHORT_DELAY, MORSE_SHORT_DELAY, MORSE_LONG_DELAY};
                flash_patern(pattern);
                break;
            }
            case 'W':
            case 'w': {
                int pattern[] = {MORSE_SHORT_DELAY, MORSE_LONG_DELAY, MORSE_LONG_DELAY};
                flash_patern(pattern);
                break;
            }
            case 'X':
            case 'x': {
                int pattern[] = {MORSE_LONG_DELAY, MORSE_SHORT_DELAY, MORSE_SHORT_DELAY, MORSE_LONG_DELAY};
                flash_patern(pattern);
                break;
            }
            case 'Y':
            case 'y': {
                int pattern[] = {MORSE_LONG_DELAY, MORSE_SHORT_DELAY, MORSE_LONG_DELAY, MORSE_LONG_DELAY};
                flash_patern(pattern);
                break;
            }
            case 'Z':
            case 'z': {
                int pattern[] = {MORSE_LONG_DELAY, MORSE_LONG_DELAY, MORSE_SHORT_DELAY, MORSE_SHORT_DELAY};
                flash_patern(pattern);
                break;
            }
            case '0': {
                int pattern[] = {MORSE_LONG_DELAY, MORSE_LONG_DELAY, MORSE_LONG_DELAY, MORSE_LONG_DELAY,
                                 MORSE_LONG_DELAY};
                flash_patern(pattern);
                break;
            }
            case '1': {
                int pattern[] = {MORSE_SHORT_DELAY, MORSE_LONG_DELAY, MORSE_LONG_DELAY, MORSE_LONG_DELAY,
                                 MORSE_LONG_DELAY};
                flash_patern(pattern);
                break;
            }
            case '2': {
                int pattern[] = {MORSE_SHORT_DELAY, MORSE_SHORT_DELAY, MORSE_LONG_DELAY, MORSE_LONG_DELAY,
                                 MORSE_LONG_DELAY};
                flash_patern(pattern);
                break;
            }
            case '3': {
                int pattern[] = {MORSE_SHORT_DELAY, MORSE_SHORT_DELAY, MORSE_SHORT_DELAY, MORSE_LONG_DELAY,
                                 MORSE_LONG_DELAY};
                flash_patern(pattern);
                break;
            }
            case '4': {
                int pattern[] = {MORSE_SHORT_DELAY, MORSE_SHORT_DELAY, MORSE_SHORT_DELAY, MORSE_SHORT_DELAY,
                                 MORSE_LONG_DELAY};
                flash_patern(pattern);
                break;
            }
            case '5': {
                int pattern[] = {MORSE_SHORT_DELAY, MORSE_SHORT_DELAY, MORSE_SHORT_DELAY, MORSE_SHORT_DELAY,
                                 MORSE_SHORT_DELAY};
                flash_patern(pattern);
                break;
            }
            case '6': {
                int pattern[] = {MORSE_LONG_DELAY, MORSE_SHORT_DELAY, MORSE_SHORT_DELAY, MORSE_SHORT_DELAY,
                                 MORSE_SHORT_DELAY};
                flash_patern(pattern);
                break;
            }
            case '7': {
                int pattern[] = {MORSE_LONG_DELAY, MORSE_LONG_DELAY, MORSE_SHORT_DELAY, MORSE_SHORT_DELAY,
                                 MORSE_SHORT_DELAY};
                flash_patern(pattern);
                break;
            }
            case '8': {
                int pattern[] = {MORSE_LONG_DELAY, MORSE_LONG_DELAY, MORSE_LONG_DELAY, MORSE_SHORT_DELAY,
                                 MORSE_SHORT_DELAY};
                flash_patern(pattern);
                break;
            }
            case '9': {
                int pattern[] = {MORSE_LONG_DELAY, MORSE_LONG_DELAY, MORSE_LONG_DELAY, MORSE_LONG_DELAY,
                                 MORSE_SHORT_DELAY};
                flash_patern(pattern);
                break;
            }
            case ' ':
            case '.': {
                delay(MORSE_LONG_DELAY * 2);
                break;
            }
        }
    }
}
