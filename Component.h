#pragma once


class Component {
public:
    virtual void setup_ros(NodeHandle& _nh) = 0;

    virtual void update() = 0;

protected:
    String hardware_name = "control/mega";
};
