#include "Ultrason.h"

Ultrason::Ultrason(uint8_t _pin_trigger, uint8_t _pin_echo, int _max_distance, String _topic_name) {
    sonar = new NewPing(_pin_trigger, _pin_echo, _max_distance);
    topic_name = hardware_name + "/ultrason/" + _topic_name + "/distance";
}

Ultrason::~Ultrason() {
    delete distance_pub;
}

void Ultrason::setup_ros(NodeHandle& _nh) {

    distance_pub = new ros::Publisher(topic_name.c_str(), &distance_msg);
    _nh.advertise(*distance_pub);
}

void Ultrason::update() {
    distance_msg.data = sonar->ping_cm();
    distance_pub->publish(&distance_msg);
}
