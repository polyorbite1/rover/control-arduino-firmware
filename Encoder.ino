#include "Encoder.h"

#define distance_per_pulse (3.141592*wheel_diameter/pulses_per_revolution)

Encoder::Encoder(int _ChA_pin, int _ChB_pin, bool _left, String _topic_name) {
    ChA_pin = _ChA_pin;
    ChB_pin = _ChB_pin;
    left = _left;
    distance_topic_name = hardware_name + "/encoder/" + _topic_name + "/distance";
    reset_topic_name = hardware_name + "/encoder/" + _topic_name + "/reset";

    pinMode(ChA_pin, INPUT);
    pinMode(ChB_pin, INPUT);
}

Encoder::~Encoder() {
    delete publish_distance;
}

void Encoder::setup_ros(NodeHandle& nh) {

    publish_distance = new ros::Publisher(distance_topic_name.c_str(), &distance_message);
    nh.advertise(*publish_distance);

    subscribe_reset = new ros::Subscriber<std_msgs::Bool, Encoder>(reset_topic_name.c_str(), &Encoder::reset_pulses,
                                                                   this);
    nh.subscribe(*subscribe_reset);
}

void Encoder::reset_pulses(const std_msgs::Bool& msg) {
    if (msg.data)
        pulses = 0;
}

void Encoder::update() {
    update_encoder_position();
    update_ros();
}

void Encoder::update_ros() {
    distance_message.data = pulses_to_distance();
    publish_distance->publish(&distance_message);
}

int Encoder::pulses_to_distance() {
    return pulses * ((int) distance_per_pulse);
}

void Encoder::update_encoder_position() {
    if (channel_A_previous != digitalRead(ChA_pin)) {
        int change = left ? -1 : 1;
        change *= (digitalRead(ChA_pin) == 0 && digitalRead(ChB_pin) == 0) ? -1 : 1;
        change *= (digitalRead(ChA_pin) != 0 && digitalRead(ChB_pin) != 0) ? -1 : 1;

        // if change == -1: moving forward
        // if change ==  1: moving backwards
        pulses += change;
        channel_A_previous = digitalRead(ChA_pin);
    }
}