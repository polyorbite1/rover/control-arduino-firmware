#pragma once


#include <std_msgs/UInt16.h>

#include "Component.h"

class BatteryMonitor : public Component {
public:
    BatteryMonitor(int _voltage_pin, int _current_pin, String _topic_name);

    ~BatteryMonitor();

    void setup_ros(NodeHandle& _nh) override;

    void update() override;

    uint16_t getVoltage();

protected:
    int voltage_pin;
    int current_pin;

    // ros stuff
    String voltage_topic_name;
    String current_topic_name;

    ros::Publisher* voltage_publisher;
    ros::Publisher* current_publisher;

    std_msgs::UInt16 voltage_message;
    std_msgs::UInt16 current_message;

private:
};
