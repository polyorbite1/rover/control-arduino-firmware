#pragma once

#define MOTOR_RR_PWM 2
#define MOTOR_RR_ChA 22
#define MOTOR_RR_ChB 23
#define MOTOR_RR_DIR 24

#define MOTOR_RL_PWM 3
#define MOTOR_RL_ChA 25
#define MOTOR_RL_ChB 26
#define MOTOR_RL_DIR 27

#define MOTOR_CR_PWM 4
#define MOTOR_CR_ChA 28
#define MOTOR_CR_ChB 29
#define MOTOR_CR_DIR 30

#define MOTOR_CL_PWM 5
#define MOTOR_CL_ChA 31
#define MOTOR_CL_ChB 32
#define MOTOR_CL_DIR 33

#define MOTOR_FR_PWM 6
#define MOTOR_FR_ChA 34
#define MOTOR_FR_ChB 35
#define MOTOR_FR_DIR 36

#define MOTOR_FL_PWM 7
#define MOTOR_FL_ChA 37
#define MOTOR_FL_ChB 38
#define MOTOR_FL_DIR 39

#define LIGHT_FRONT_PIN 8
#define LIGHT_FRONT_INVERT_PIN 40

#define LIGHT_REAR_PIN 9
#define LIGHT_REAR_INVERT_PIN 41

#define TRIGGER_PIN 12
#define ECHO_PIN 13

#define FAN_1_PWM 10
#define FAN_1_SENSE 4 // pin 19, unchangable! (https://www.arduino.cc/reference/en/language/functions/external-interrupts/attachinterrupt/)

#define FAN_2_PWM 11
#define FAN_2_SENSE 5 // pin 18, unchangable! (https://www.arduino.cc/reference/en/language/functions/external-interrupts/attachinterrupt/)

#define SCIENCE_DRILL_PWM_PIN 44
#define SCIENCE_DRILL_DIR_PIN 47
#define SCIENCE_LIFT_PWM_PIN 45
#define SCIENCE_LIFT_DIR_PIN 48

#define GPS_RX_PIN 18
#define GPS_TX_PIN 19

#define BNO_ID 55
#define BNO_ADDRESS 0x28
