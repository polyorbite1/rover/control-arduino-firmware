//
// Created by Gaetan Florio on 2023-02-02.
//

// TODO: Test Ultrason

#ifndef CONTROL_ARDUINO_FIRMWARE_CAPTEUR_H
#define CONTROL_ARDUINO_FIRMWARE_CAPTEUR_H

#include <std_msgs/Float32.h>

#include "Component.h"
#include "NewPing.h" // arduino ide install NewPing
// If unable: https://bitbucket.org/teckel12/arduino-new-ping/wiki/Home

class Ultrason : public Component {
public:
    Ultrason(uint8_t _pin_trigger, uint8_t _pin_echo, int _max_distance, String _topic_name);

    ~Ultrason();

    void setup_ros(NodeHandle& _nh) override;

    void update() override;

private:
    NewPing* sonar;
    std_msgs::Float32 distance_msg;
    ros::Publisher* distance_pub;
    String topic_name;

};

#endif //CONTROL_ARDUINO_FIRMWARE_CAPTEUR_H
