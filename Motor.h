#pragma once

#include <std_msgs/Int8.h>

#include "Component.h"

class Motor : public Component {
public:
    Motor(int _PWM_pin, int _Dir_pin, bool _left, String _topic_name);

    ~Motor();

    void setup_ros(NodeHandle& _nh) override;

    void update() override;

protected:
    void move_at(const std_msgs::Int8& msg);

    int PWM_pin;
    int Dir_pin;

    bool left;

    byte speed;

    // ros stuff
    String topic_name;

    ros::Subscriber <std_msgs::Int8, Motor>* subscribe_speed;
};
