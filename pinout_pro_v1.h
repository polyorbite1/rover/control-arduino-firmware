#pragma once

// Motors
#define MOTOR_FL_PWM 6
#define MOTOR_FL_DIR 23

#define MOTOR_FR_PWM 44
#define MOTOR_FR_DIR 48

#define MOTOR_CL_PWM 5
#define MOTOR_CL_DIR 22

#define MOTOR_CR_PWM 46
#define MOTOR_CR_DIR 47

#define MOTOR_RL_PWM 4
#define MOTOR_RL_DIR 24

#define MOTOR_RR_PWM 45
#define MOTOR_RR_DIR 49

// Encoders
#define MOTOR_FL_ChA 25
#define MOTOR_FL_ChB 27

#define MOTOR_FR_ChA 52
#define MOTOR_FR_ChB 53

#define MOTOR_CL_ChA 30
#define MOTOR_CL_ChB 31

#define MOTOR_CR_ChA 43
#define MOTOR_CR_ChB 41

#define MOTOR_RL_ChA 28
#define MOTOR_RL_ChB 29

#define MOTOR_RR_ChA 40
#define MOTOR_RR_ChB 42

// Battery monitoring
#define VOLTAGE_READING_PIN A0
#define CURRENT_READING_PIN A1

// Switches
#define SPARE 32
#define SPARE2 33
#define LIGHT_BLUE 34
#define LIGHT_FRONT 35 ///////////
#define LIGHT_RED 36
#define LIGHT_ORANGE 37
#define LIGHT_BUZZER 38
#define LIGHT_GREEN 39

// GPS
#define GPS_Tx 16
#define GPS_Rx 17

// IMU
#define BNO_ID 55
#define BNO_ADDRESS 0x28

// Fans
#define FAN_1_PWM 10
#define FAN_1_INT  2

#define FAN_2_PWM 11
#define FAN_2_INT  3

#define FAN_3_PWM 12
#define FAN_3_INT 19

#define FAN_4_PWM 13
#define FAN_4_INT 18
